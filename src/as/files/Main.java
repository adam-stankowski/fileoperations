package as.files;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main {
  // TODO zrobic tak zeby mozna bylo sprawdzic dowolna sciezke - absolutna i
  // relatywna
  // TODO zrobic tak zeby wiedziec czy znalazlem plik czy folder

  private static void usage() {
    System.out.println("Hmm you're probably starting the application wrong.");
    System.out.println("Here's how to do it properly");
    System.out.println("\t jfind *.java -d directory");
  }

  public static void main(String[] args) throws IOException {

    if (args.length < 3 || !args[1].equals("-d")) {
      usage();
      return;
    }
    Path startingPath = Paths.get(args[2]).toAbsolutePath().normalize();
    String glob = args[0];
    FileFinder f = new FileFinder();
    List<Path> result = f.findFile(glob, startingPath);
    System.out.printf("Found %d files %s in %s%n", result.size(), glob,
        startingPath);
    for (Path p : result) {
      System.out.println(p.toString());
    }
  }

}
