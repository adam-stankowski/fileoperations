package as.files;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public final class FileFinder {

  public FileFinder() {
  }

  public List<Path> findFile(String glob, Path startingPath) throws IOException {
    FilePathChecker f = new FilePathChecker(glob);
    Files.walkFileTree(startingPath, f);
    return f.getFoundList();
  }

  private final class FilePathChecker extends SimpleFileVisitor<Path> {
    private final PathMatcher matcher;
    private final List<Path> foundFiles;

    FilePathChecker(String glob) {
      if (glob == null || glob.equals("")) {
        throw new IllegalArgumentException("Illegal find pattern");
      }
      this.matcher = FileSystems.getDefault().getPathMatcher("glob:" + glob);
      this.foundFiles = new ArrayList<Path>();
    }

    private void scan(Path file, BasicFileAttributes attrs) {

      Path filename = file.getFileName();
      if (filename != null && matcher.matches(filename)) {
        foundFiles.add(filename);
      }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        throws IOException {

      scan(file, attrs);
      return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
        throws IOException {

      scan(dir, attrs);
      return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc)
        throws IOException {
      System.err.println(exc.getMessage());
      return FileVisitResult.CONTINUE;
    }

    public List<Path> getFoundList() {
      return new ArrayList<>(foundFiles);
    }

  }

}
